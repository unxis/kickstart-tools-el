# Kickstart Tools for Enterprise Linux

This project contains scripts and configurations for automating the process
to build custom installations images of [Enterprsie Linux](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux)
(EL) and its child distributions ([CentOS](https://www.centos.org),
[OL](https://www.oracle.com/linux/)).  Although there can be more convenient
methods of automating installation, this method can be useful on a small scale
or for environments where network resources are limited.

*This is a rewrite of an earlier idea tool that I maintained in the RHEL 4/5 era.*

It is important to note that these tools do not create custom configurations
for Anaconda but simply automate the process of generating a valid image in a
versioned and easily repeatable manner.

Several example Kickstart configurations used in my environment are included
with this package.

> ***BEFORE YOU START:*** *The current version of these tools perform no input
validation. Use accordingly!*
>
> *See: issue #1 & issue #2 for further details*
 
# Requirements

> ***CAUTION:*** *I've attempted not to break anything catastrophically, however
> these tools have are undergone minimal testing outside my own environment.
> Currently they assume correct usage.*
>
> *My tested build environments are minimal installations of Debian 9, Debian 10,
> Ubuntu 18.04, and CentOS 7.*

In order to build a new ISO with an embedded Anaconda installer configuration
via kickstart, you need a modern Linux distribution with the `mkisofs` and
`implantisomd5` binaries installed.

* Debian based distribution, run: `apt install genisoimage isomd5sum`
* EL based distributions, run: `yum install genisoimage isomd5sum`

Since we're not actually compling any software or building EL native packages
the build platform is not important.

The build script will verify that these tools are installed prior to running.

In order to create your image you will also require original source media.  Any
distribution ISO should work with this tool, however minimal and network boot
ISOs require additional attention when building kickstart configuration file as
key software may not be available at install time and/or special consideration
of network configuration may be required.

For the purposes of documentation assume CentOS Minimal ISO as the base image.
Download link can be found in the [Reference](#reference) section.

# Workflow

The workflow for these tools involves three steps.

### 1. Prepare workspace

The general workflow for these tools is to clone the project, then to prepare
a workspace using the ``prep-workspace.sh``.  This creates a copy of the
contents of the specified distribution ISO that can be modified with local
customizations.

### 2. Customize local configuration

You can modify the Kickstart configuration by adding assets to the ``./kickstart/files/``
directory.  Additonal changes can be made to the Kickstart configurations
and boot configuration as required.

Once you are happy with the contents of your image you can copy your
customization to the image workspace using ``update-image.sh``.

This script will embed the current verison of HEAD in the git repository on the
ISO at ``$ISOROOT/kickstart-meta/image-revision``.  Eventually we will check
and warn if the repo directory contains changes that have not been committed to
git yet.

You can run this step repeatedly during testing to push new configurations to
the image.

> ***NOTE:*** This version of the tools does not remove old configuration from
image workspace.  It should be fairly trivial to puge configs, supplemental
files, and metadata but some thought should go into how we push the ISO boot
configs

### 3. Building custom image

The custom image is built from the workspace by running the ``build-image.sh``
script.  Each build will have an updated time stamp stored in
``$ISOROOT/kickstart-meta/build-timestamp`` at build time.

## Workflow summary

1. Switch to build directory: ``cd bin``
2. Extract ISO using: ``./prep-workspace.sh -f el-install-image.iso``
3. Edit config in git work directory.
4. Check in changes with git.
5. Push config to work directory: ``./update-image.sh``.
6. Build image from work directory: ``./build-image.sh -n "ImageName1"``

# Configuration

All configuration can be found in the ``conf/common``.  Some configuration
options can be overridden 

# Usage

**Note:** *It is strongly advised to run all commands from within the ``bin``
directory.  Currently thre are embedded relative paths that may not function
correctly and have not been properly fixed/tested.*

### prep-workspace.sh

``prep-workspace.sh`` - prepare initial workspace for building ISOs

Command line arguments and usage:

```
Usage: ./prep-workspace.sh [OPTIONS] -f /path/to/el/install.iso

  -f <source iso>	MANDATORY: source ISO as argument
  -c <config file>	specify location of config file as argument
  -d <workspace>	manually specify build workspace as argument
  -m <mount point>	manually specify ISO mount point used temporarily by prep-workspace.sh
  -q			quiet mode
  -h			this help information
```

### update-image.sh

``update-image.sh`` - push configuration from repo to ISO workspace

Command line arguments and usage:

```
Usage: ./update-image.sh [OPTIONS]

  -c <config file>	specify location of config file as argument
  -d <workspace>	manually specify build workspace as argument
  -q			quiet mode
  -h			this help information
```

### build-image.sh

``build-image.sh`` - build ISO from modified sources

Command line arguments and usage:

```
Usage: ./build-image.sh [OPTIONS]

  -c <config file>	specify location of config file as argument
  -d <workspsace> 	manually specify build workspace as argument
  -n "MYBUILD1"		specify image name as argument
  -q			quiet mode
  -h			this help information
```

# Bug reports and help

Issues can be reported via GitLab [here](https://gitlab.com/unxis/kickstart-tools-el/issues).

> ***NOTE:*** *The current versions of the tools are essentially a
> proof-of-concept, with the roughest edges sanded off. It is my intention to
> rewrite the tools in a more integrated and idiot-proof manner, but for the
> time being they serve my purpose: image reproducibility and audit trail.*
>
> *This notice will be removed when no longer relevant.*
>
> *Please be careful!*

## Reference

* [Red Hat Enterprise Linux 7 Installation Guide: 26.3. Kickstart Syntax Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/sect-kickstart-syntax)
* [Red Hat Customer Portal: Anaconda Customization Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html-single/anaconda_customization_guide/index)
* [CentOS 7 1810 Minimal ISO download mirror](http://isoredirect.centos.org/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-1810.iso)

#### Industrial robot factory icon license

The [Industrial Robot Factory PNG Icon](https://www.pngrepo.com/svg/192630/industrial-robot-factory)
is used under a [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
license.