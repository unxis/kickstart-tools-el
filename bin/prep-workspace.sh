#!/bin/bash
#
# prep-workspace.sh - prepare initial workspace for building ISOs
#
# Copyright 2019 Gabriel O'Brien
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# let's try to find a config file
#
# this deals with issues related to absolute paths in Bash, especially when
# running directly from the Git repostiory working directory and allows for
# use of relative paths
#
# see: http://mywiki.wooledge.org/BashFAQ/028
#
# we don't exit right away in order to allow for usage output

if [ -f ../conf/kickstart-tools-el.conf ]; then
  configfile="../conf/kickstart-tools-el.conf"
  mode=git
elif [ -f ~/.kickstart-tools-el.conf ]; then
  configfile="~/.kickstart-tools-el.conf"
  mode=home
elif [ -f /etc/kickstart-tools-el.conf ]; then
  configfile="/etc/kickstart-tools-el.conf"
  mode=etc
else
  configfile="NULL"
fi

# print usage in-line
printHelp() {
cat << EOUSAGE
Usage: ./prep-workspace.sh [OPTIONS] -f /path/to/el/install.iso

  -f <source iso>   MANDATORY: source ISO as argument
  -c <config file>  specify location of config file as argument
  -d <workspace>    manually specify build workspace as argument
  -m <mount point>  manually specify ISO mount point used temporarily by
                    the prep-workspace.sh script
  -q                quiet mode
  -h                this help information

Home page: https://gitlab.com/unxis/kickstart-tools-el
EOUSAGE
}

# get our options
while getopts c:f:m:d:qh opt; do
  case $opt in
  c)
      configfile=$OPTARG
      ;;
  f)
      myisofile=$OPTARG
      ;;
  m)
      mymountpoint=$OPTARG
      ;;
  d)
      myworkspaceroot=$OPTARG
      ;;
  q)
      v=false
      ;;
  h)
      printHelp
      exit 0
      ;;
  esac
done
shift $((OPTIND - 1))

# read configuration

if [ $configfile == "NULL" ]; then
  printf 'ERROR: no valid configuration file found in expected location(s).\n'
  exit 65
elif [ ! -f $configfile ]; then
  printf 'ERROR: unable to read configuration file %s\nExiting.\n' $configfile
  exit 65
fi

source $configfile

# if running from git workdir, switch to project root
[ $mode == "git" ] && cd ..

# replace common config with getopts values
[ -z $myworkspaceroot ] || workspace=$myworkspaceroot
[ -z $myisofile ] || isofile=$myisofile
[ -z $mymountpoint ] || mountpoint=$mymountpoint

# set a righteous mask
umask 0022

# build work directory

# mount iso
$v && printf 'Mounting source distribution ISO from: %s\n' $isofile
$v && printf 'Mount point: %s\n' $mountpoint
sudo mkdir -p $mountpoint
sudo mount -t iso9660 -o loop,ro $isofile $mountpoint

# copy iso contents to work directory
$v && printf 'Copying ISO contents to: %s\n' $workspaceroot
$v && rsync -ah --info=progress2 ${mountpoint}/ $workspaceroot
$v || rsync -a ${mountpoint}/ $workspaceroot

# cleanup
$v && printf 'Unmounting source distribution ISO.\n'
sudo umount $mountpoint && sudo rmdir $mountpoint

# done
exit 0
