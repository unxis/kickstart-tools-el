#!/bin/bash
#
# build-image.sh - build ISO from modified sources
#
# Copyright 2019 Gabriel O'Brien
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# let's try to find a config file
#
# this deals with issues related to absolute paths in Bash, especially when
# running directly from the Git repostiory working directory and allows for
# use of relative paths
#
# see: http://mywiki.wooledge.org/BashFAQ/028
#
# we don't exit right away in order to allow for usage output

if [ -f ../conf/kickstart-tools-el.conf ]; then
  configfile="../conf/kickstart-tools-el.conf"
  mode=git
elif [ -f ~/.kickstart-tools-el.conf ]; then
  configfile="~/.kickstart-tools-el.conf"
  mode=home
elif [ -f /etc/kickstart-tools-el.conf ]; then
  configfile="/etc/kickstart-tools-el.conf"
  mode=etc
else
  configfile="NULL"
fi

# print usage in-line
printHelp() {
cat << EOUSAGE
Usage: ./build-image.sh [OPTIONS]

  -c <config file>  specify location of config file as argument
  -d <workspsace>   manually specify build workspace as argument
  -n "MYBUILD1"     specify image name as argument
  -q                quiet mode
  -h                this help information

Home page: https://gitlab.com/unxis/kickstart-tools-el
EOUSAGE
}

# get our options
while getopts c:n:d:qh opt; do
  case $opt in
  c)
      configfile=$OPTARG
      ;;
  n)
      myimagename=$OPTARG
      ;;
  d)
      myworkspaceroot=$OPTARG
      ;;
  q)
      v=false
      ;;
  h)
      printHelp
      exit 0
      ;;
  esac
done

shift $((OPTIND - 1))

# read configuration

if [ $configfile == "NULL" ]; then
  printf 'ERROR: no valid configuration file found in expected location(s).\n'
  exit 65
elif [ ! -f $configfile ]; then
  printf 'ERROR: unable to read configuration file %s\nExiting.\n' $configfile
  exit 65
fi

source $configfile

# if running from git workdir, switch to project root
[ $mode == "git" ] && cd ..

# replace common config with getopts values                                                             
[ -z $myimagename ] || imagename=$myimagename
[ -z $myworkspaceroot ] || workspace=$myworkspaceroot

# set a righteous mask
umask 0022

# get config revision
if [ ! -f ${workspaceroot}/${ksmeta}/image-revision ]; then
  printf 'ERROR: Unable to read image revision from workspace.\nExiting.\n'
  exit 66
fi
revision=$(cat ${workspaceroot}/${ksmeta}/image-revision)

# create and write timestamp for build
stamp=`date +%Y%m%d.%H%M%S`
echo $stamp > ${workspaceroot}/${ksmeta}/build-timestamp

# generate build name
buildname="$imagename $revision"

# generate ISO file name
ins="${imagename// /-}"
isofilename="${ins,,}-${stamp}-${revision}.iso"
isofull="/tmp/${isofilename}"

# build image
buildISO() {
  genisoimage -U -r -v -T -J -input-charset utf8 -joliet-long -V "$buildname" \
    -volset "$buildname" \
    -A "$buildname" \
    -b isolinux/isolinux.bin -c isolinux/boot.cat \
    -no-emul-boot -boot-load-size 4 -boot-info-table \
    -eltorito-alt-boot -e images/efiboot.img -no-emul-boot -o ../${isofilename} .
}
$v && printf 'Building image: %s\n' "$buildname"
(cd $workspaceroot && buildISO)

# embed checksum
implantisomd5 $isofull

# print file location when complete
$v && printf 'ISO saved as: %s\n' $isofull

exit 0
