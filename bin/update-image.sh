#!/bin/bash
#
# update-image.sh - push configuration from repo to ISO workspace
#
# Copyright 2019 Gabriel O'Brien
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# let's try to find a config file
#
# this deals with issues related to absolute paths in Bash, especially when
# running directly from the Git repostiory working directory and allows for
# use of relative paths
#
# see: http://mywiki.wooledge.org/BashFAQ/028
#
# we don't exit right away in order to allow for usage output

if [ -f ../conf/kickstart-tools-el.conf ]; then
  configfile="../conf/kickstart-tools-el.conf"
  mode=git
elif [ -f ~/.kickstart-tools-el.conf ]; then
  configfile="~/.kickstart-tools-el.conf"
  mode=home
elif [ -f /etc/kickstart-tools-el.conf ]; then
  configfile="/etc/kickstart-tools-el.conf"
  mode=etc
else
  configfile="NULL"
fi

# print usage in-line

printHelp() {
cat << EOUSAGE
Usage: ./update-image.sh [OPTIONS]

  -c <config file>  specify location of config file as argument
  -d <workspace>    manually specify build workspace as argument
  -q                quiet mode
  -h                this help information

Home page: https://gitlab.com/unxis/kickstart-tools-el
EOUSAGE
}

# get our options

while getopts c:d:qh opt; do
  case $opt in
  c)
      configfile=$OPTARG
      ;;
  d)
      myworkspaceroot=$OPTARG
      ;;
  q)
      v=false
      ;;
  h) 
      printHelp
      exit 0
      ;;
  esac
done
shift $((OPTIND - 1))

# read configuration

if [ $configfile == "NULL" ]; then
  printf 'ERROR: no valid configuration file found in expected location(s).\n'
  exit 65
elif [ ! -f $configfile ]; then
  printf 'ERROR: unable to read configuration file %s\nExiting.\n' $configfile
  exit 65
fi

source $configfile

# if running from git workdir, switch to project root
[ $mode == "git" ] && cd ..

# replace common config with getopts values

[ -z $myworkspaceroot ] || workspaceroot=$myworkspaceroot

# set a righteous mask

umask 0022

# generic function to set image revision

getImageRev() {
  # our image revision is our git version
  # you can replace this method with an alternative method of generating
  # revision so long as it print a text string to stdout
  git rev-parse --short HEAD
}

# adding repository version to image

setImageMetadata() {
  imagerev=$(getImageRev)
  if [ -f ${1} ]; then
    oldimagerev=`cat ${1}`
    $v && printf 'Workspace contains existing kickstart build: %s\n' $oldimagerev
    $v && printf 'Overwriting with current build: %s\n' $imagerev
  fi
  $v && printf 'Adding version to image.\n'
  echo "$imagerev" > ${1}
}

# add build metadata

mkdir -p ${workspaceroot}/${ksmeta}
setImageMetadata ${workspaceroot}/${ksmeta}/image-revision

# write files to ISO image

$v && printf 'Copying configuration to ISO work directory...\n'
rsync -a ${isofilessrc}/ ${workspaceroot}
$v && printf 'done!\n'

# fin

exit 0
